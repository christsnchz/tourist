import React from "react";
import Header from "../../components/Header";
import SearchView from "../../components/SearchView";

const Search = () => {
  return (
    <>
      <Header fixed={true} />

      <SearchView />
    </>
  );
};

export default Search;
